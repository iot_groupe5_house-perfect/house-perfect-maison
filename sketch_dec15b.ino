#include <WiFi.h>
#include <PubSubClient.h>
extern "C" {
  #include "freertos/FreeRTOS.h"
  #include "freertos/timers.h"
}
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "sensor.h"
#include "ecran.h"
#include "ledControl.h"
#include "fanControl.h"
#include "PIRDetector.h"
#include <SPI.h>
#include "nfcScanner.h"
#include "button.h"
#include "RGBControl.h"
#include <WiFiManager.h> 


// Replace with your network credentials (STATION)
const char* ssid = "maurin-wifi";
const char* Password = "WIFI-BINOUZE";
//const char* ssid = "Atelier";
//const char* Password = "Atelier1PP";

// MQTT Broker
const char *mqtt_broker = "broker.hivemq.com";
const char *topic = "test/flower";
const char *mqtt_username = "emqx";
const char *mqtt_password = "public";
const int mqtt_port = 1883;


#define STEAM_SENSOR_PIN 34
#define BUZZER_PIN 25
#define WINDOW_PIN 5
#define DOOR_PIN 13


WiFiClient espClient;
PubSubClient client(espClient);
WiFiManager wifiManager;

void MQTTcallback(char *topic, byte *payload, unsigned int length) {
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char) payload[i]);
  }
  Serial.println();
  Serial.print("length : ");
  Serial.println(length);
  Serial.println("-----------------------");
  if (strcmp(topic, "house-perfect/rgb") == 0) {
    char *cstr = new char[length + 1];
    for (int i = 0; i < length; i++) {
      cstr[i] = payload[i];
    }
    cstr[length] = '\0';
    //strncpy(cstr, String((char *)payload).c_str(), length);
    //strncpy(cstr, (char *)payload, length);

    refreshRGB();
    delay(1000);
    controlRGB(cstr);
    Serial.println(cstr);
    Serial.println("zizi");
    //delete [] cstr;
  }
  if (strcmp(topic, "house-perfect/led") == 0) {
    char *cstr = new char[length + 1];
    for (int i = 0; i < length; i++) {
      cstr[i] = payload[i];
    }
    cstr[length] = '\0';
    if (strcmp(cstr, "on") == 0) {
      pilotLED(true);
    }
    
  }
}

void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, Password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void initMQTT() {
  //connecting to a mqtt broker
  client.setServer(mqtt_broker, mqtt_port);
  client.setCallback(MQTTcallback);
  while (!client.connected()) {
    String client_id = "my-esp32-client-";
    client_id += String(WiFi.macAddress());
    Serial.printf("The client %s connects to the public mqtt broker\n", client_id.c_str());
    if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
      Serial.println("Public emqx mqtt broker connected");
    } else {
      Serial.print("failed with state ");
      Serial.println(client.state());
      delay(2000);
    }
  }  
}

void helloMQTT() {
  // publish and subscribe
  client.publish(topic, "Hi EMQX I'm ESP32 ^^");
  client.subscribe(topic);
  client.subscribe("house-perfect/rgb");
}

void setup() {
  Serial.begin(115200);
  wifiManager.autoConnect("HOUSE-PERFECT", "HOUSE-PERFECT");
  Wire.begin(); 
  //initWiFi();
  Serial.print("RRSI: ");
  Serial.println(WiFi.RSSI());
  initMQTT();
  helloMQTT();
  setupDHT11();
  setupGasIndicator();
  setupSreen();
  setupLED();
  setupFan();
  setupPIRDetection();
  setupNFCScanner();
  setupButton();
  setupRGB();
  refreshRGB();
}

void publishFloatToServer(const char* topic, const float data) {
  char dataChar[5];
  dtostrf(data, 5, 2, dataChar);
  client.publish(topic, dataChar);
}

void publishIntToServer(const char* topic, const int data) {
  char dataChar[5];
  dtostrf(data, 5, 2, dataChar);
  client.publish(topic, dataChar);
}





void loop() {
  delay(5000);
  Serial.println("new line");
  Serial.println("");
  Serial.println("----------");
  Serial.println("");
  Serial.println("");
  //TEMPERATURE AND HUMIDITY BLOCK
  float tempe = getTemperature();
  float humi = getHumidity();
  publishFloatToServer("house-perfect/humidity", humi);
  publishFloatToServer("house-perfect/temperature", tempe);
  Serial.print("temperature : ");
  Serial.println(tempe);
  Serial.print("humidity : ");
  Serial.println(humi);

  //GAS BLOCK
  bool gas = getGasIndicator();
  Serial.print("Gas Value: ");
  Serial.println(gas);
  const char* vOutGas = gas ? "false" : "true";
  client.publish("house-perfect/gas", vOutGas);

  //IP BLOCK
  const char* ip = WiFi.localIP().toString().c_str();
  writeScreen(0,0, ip);

  //LED BLOCK
  //pilotLED(true);

  //FAN BLOCK
  pilotFan(true, 50);

  //PIRdetection BLOCK
  bool detec = PIRDetection();
  Serial.print("detection : ");
  Serial.println(detec);
  const char* vOutDetection = detec ? "true" : "false";
  client.publish("house-perfect/detection", vOutDetection);
  //PIRDetection(); // false == aucune detection // true == detection de quelqu'un

  //RGB LED BLOCK
  //refreshRGB();
  //delay(1000);
  //controlRGB("blue");

  //RFID BLOCK
  //Serial.print("access : ");
  //Serial.println(readUID());

  //LOOP MQTT
  client.loop();
}



